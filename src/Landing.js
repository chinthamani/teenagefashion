import React, { Component } from 'react';
import Header from './Common/Header';
import Footer from './Common/Footer';
import Cart from './Cart/Cart';
import Home from './Home/Home';
import './resources/css/animate.css';
import './resources/css/style.css';
import './resources/css/owl.carousel.min.css';
import './resources/css/slicknav.min.css';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import './resources/js/main';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Categories from './Common/Categories';
import Product from './Products/Product';
import Contact from './Contact/Contact';
import Orders from './Orders/Orders';

import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { getCategories } from './Common/CommonActions';




class Landing extends Component {
    constructor(props) {
        super(props);
        this.props.getCategories();
        this.state = {
            categoryList: []
        };
    }



    componentWillReceiveProps(nextProps) {
        console.log("nextProps");
        console.log(nextProps);
        this.setState({
            categoryList: nextProps.categoryList.catList
        });
    }

    render() {
        return (
            <div className="Landing">
                <Router>
                    <Header categoryList={this.state.categoryList} />
                    <Route exact path="/" component={Home} />
                    <Route exact path="/Home" component={Home} />
                    <Route path="/Cart" component={Cart} />
                    <Route path="/Categories/:id" component={Categories} />
                    <Route path="/Product" component={Product} />
                    <Route path="/Contact" component={Contact} />
                    <Route path="/Orders" component={Orders} />
                    
                    <Footer />
                </Router>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log("mapstateToProps");
    console.log(state);
    return {
        categoryList: state.category
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getCategories
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
