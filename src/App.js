import React from 'react';
import Landing from './Landing';
import { store } from './Common/Store';
import {Provider} from 'react-redux';
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Landing />
      </Provider>
    </div>
  );
}

export default App;
