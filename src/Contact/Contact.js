import React from 'react';

import Breadcrumb from '../Common/Breadcrumb';

function Contact() {
    return (
        <div className="Contact">
            <Breadcrumb title="Contact" />
            <div className="p-1">
                <section className="contact-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 contact-info">
                                <h3>Get in touch</h3>
                                <p>4th cross street,venkateshwara nagar,ramapuram,chennai-6000312</p>
                                <p>8838141286</p>
                                <p>admin@teenagefashion.com</p>
                                <div className="contact-social">
                                    <a href="/"><i className="fa fa-pinterest"></i></a>
                                    <a href="/"><i className="fa fa-facebook"></i></a>
                                    <a href="/"><i className="fa fa-twitter"></i></a>
                                    <a href="/"><i className="fa fa-dribbble"></i></a>
                                    <a href="/"><i className="fa fa-behance"></i></a>
                                </div>

                            </div>
                            <div className="col-lg-6 contact-info">
                                <form className="contact-form">
                                    <input type="text" placeholder="Your name" />
                                    <input type="text" placeholder="Your e-mail" />
                                    <input type="text" placeholder="Subject" />
                                    <textarea placeholder="Message"></textarea>
                                    <button className="site-btn">SEND NOW</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}
export default Contact;

