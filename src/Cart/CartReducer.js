function CartReducer(state = {}, actions) {

    switch (actions.type) {
        case "ADD_TO_CART_SUCCESS":
            console.log({cartList: actions.payload,isLoading:false});
            return state ={cartList: actions.payload,isLoading:false};
        case "CART_LOADING":
            return state= {
                isLoading : true,
                cartList : []
            };
        default: return state;
    }
}

export default CartReducer;
