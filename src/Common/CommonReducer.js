function CommonReducer(state = {}, actions) {

    switch (actions.type) {
        case "GET_CATEGORIES_SUCCESS":
            return state={catList:actions.payload,isLoading:false};
            case "CATEGORY_LOADING":
                return state={catList:[],isLoading:true};
        default: return state;
    }
}

export default CommonReducer;
