import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk'; 
import logger from 'redux-logger'
import { reducer } from './Reducer';

let middleWares = applyMiddleware(logger, thunk);
const store = createStore(reducer, middleWares);

export {store};
