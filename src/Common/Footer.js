import React from 'react';
import { Link } from 'react-router-dom';
function Footer() {
    return (
        <section className="footer-section">
            <div className="container">
                <div className="footer-logo text-center">
                    <a href="index.html"><img src="./img/logo-light.png" alt="" /></a>
                </div>
                <div className="row">
                    <div className="col-lg-6 col-sm-6">
                        <div className="footer-widget about-widget">
                            <h2>About</h2>
                            <p style={{ textAlign: 'justify' }}>Teenage Fashion enable customers to buy trendy clothes. We're dedicated to giving you the very best of clothes, with a focus on  dependability, customer service and uniqueness.
                            Teenage Fashion is a trend-setter of new age.
                            </p>

                            <img src="img/cards.png" alt="" />
                        </div>
                    </div>
                    <div className="col-lg-6 col-sm-6">
                        <div className="footer-widget contact-widget">
                            <h2>Contact</h2>
                            <div className="con-info">
                                <p>Teenage Fashion</p>
                                <p> Mobile : <a href="tel:+91-8838141286">+91 8838 141 286</a></p>
                                <p>Email : <a href="mailto:admin@theteenagefashion.in">admin@theteenagefashion.in</a></p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div className="social-links-warp">
                <div className="container" style={{ textAlign: 'center' }}>
                    <div className="social-links">
                        <Link to="/" className="instagram"><i className="fa fa-instagram"></i><span>instagram</span></Link>
                        <Link to="/" className="facebook"><i className="fa fa-facebook"></i><span>facebook</span></Link>
                        <Link to="/" className="whatsapp"><i className="fa fa-facebook"></i><span>whatsapp</span></Link>
                    </div>
                    <p className="text-white text-center mt-5">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Teenage Fashion</p>
                </div>
            </div>
        </section>
    );
}
export default Footer;