import React from 'react';
import ProductFilter from '../Products/ProductFilter';
import ProductList from '../Products/ProductList';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { getProducts } from '../Products/ProductActions';
import {addToCart} from '../Cart/CartActions';


class Categories extends React.Component {
    constructor(props) {
        super(props);
        const id = this.props.location.state.id;
       // const id = this.props.match.params.id;
        this.props.getProducts(id);
        
        this.state = {
            productList: [],
            categoriesList:[],
            cartList:[]
        }
    }

    componentWillReceiveProps(nextProps) {
        const id = nextProps.location.state.id;
       // const id = nextProps.match.params.id;
        if(this.props.location.state.id!==nextProps.location.state.id){
            this.props.getProducts(id);
        }
     
       let prodList =  Object.keys(nextProps.productList).length === 0 && nextProps.productList.constructor === Object?[]:nextProps.productList;
       let categoryList =  Object.keys(nextProps.categoriesList).length === 0 && nextProps.categoriesList.constructor === Object?[]:nextProps.categoriesList;
       let cartList =  Object.keys(nextProps.cartList).length === 0 && nextProps.cartList.constructor === Object?[]:nextProps.cartList;
       this.setState({
            productList: nextProps.productList === undefined ? [] : prodList,
            categoriesList : nextProps.categoriesList===undefined ? [] : categoryList,
            cartList : nextProps.cartList===undefined ? [] : cartList
        });

    }

    addToCart(e,id){
        e.preventDefault();
        
        this.props.addToCart(id,this.state.cartList);
    }

    render() {
        return (
            <div className="Categories">
                <div className="container-fluid">
                    <div className="row mx-3 my-3 prodFilter">
                        <div className="col-lg-3 order-1 order-lg-1">
                            <ProductFilter catList={this.state.categoriesList.catList} isLoading={this.state.categoriesList.isLoading} />
                        </div>
                        <div className="col-lg-9  order-2 order-lg-2 mb-5 mb-lg-0">
                            <ProductList addToCart={(e,id)=>this.addToCart(e,id)} prodList={this.state.productList.prodList===null?[]:this.state.productList.prodList} isLoading={this.state.productList.isLoading} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
   
    return {
        productList: state.product,
        categoriesList : state.category,
        cartList : state.cart
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getProducts,addToCart
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);