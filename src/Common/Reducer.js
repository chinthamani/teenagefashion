import { combineReducers } from 'redux';
import CommonReducer from './CommonReducer';
import ProductReducer from '../Products/ProductReducer';
import CartReducer from '../Cart/CartReducer';

export const reducer = combineReducers({
    category : CommonReducer ,
    product : ProductReducer,
    cart : CartReducer
});