import React from 'react';
import {Link} from 'react-router-dom';

function Breadcrumb(props){
	
    return(
        <div className="page-top-info m-0">
		<div className="container">
			<h4>{props.title}</h4>
			<div className="site-pagination">
				<Link to="/Home">Home</Link> 
				&rarr;
				{props.title}
			</div>
		</div>
	</div> 
    );
}
export default Breadcrumb;