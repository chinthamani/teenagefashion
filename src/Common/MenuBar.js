import React, { Component } from 'react';
import {
    MDBNavbar, MDBNavbarNav, MDBNavItem, MDBNavbarToggler, MDBCollapse,
    MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem
} from 'mdbreact';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom';
export default class MenuBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (<MDBNavbar dark expand="md" style={{ background: '#282828' }}>

            <MDBNavbarToggler onClick={this.toggleCollapse} />
            <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                <MDBNavbarNav style={{margin:'0 50px'}} left>
                <MDBNavItem>
                        <MDBDropdown>
                            <MDBDropdownToggle nav caret>
                                <span className="mr-2">Male</span>
                            </MDBDropdownToggle>
                            <MDBDropdownMenu className="dropdown-default">
                                {this.props.categoryList.map((datas, index) => {
                                    const { id, name, type } = datas;
                                    return (
                                        type === 'M' ? <MDBDropdownItem key={id} componentclass='' style={{ padding: '0px' }}>
                                            <NavLink tabIndex="0" className="dropdown-item" activeClassName="active" to={{
                                                pathname: `/Categories/${name}`,
                                                state: {
                                                    id
                                                }
                                            }}>{name}</NavLink>
                                        </MDBDropdownItem> : null
                                    );
                                })
                                }
                               
                            </MDBDropdownMenu>
                        </MDBDropdown>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBDropdown>
                            <MDBDropdownToggle nav caret>
                                <span className="mr-2">Female</span>
                            </MDBDropdownToggle>
                            <MDBDropdownMenu className="dropdown-default">
                                {this.props.categoryList.map((datas, index) => {
                                    const { id, name, type } = datas;
                                    return (
                                        type === 'F' ? <MDBDropdownItem key={id} componentclass='' style={{ padding: '0px' }}>
                                            <NavLink tabIndex="0" className="dropdown-item" activeClassName="active" to={{
                                                pathname: `/Categories/${name}`,
                                                state: {
                                                    id
                                                }
                                            }}>{name}</NavLink>
                                        </MDBDropdownItem> : null
                                    );
                                })
                                }
                               
                            </MDBDropdownMenu>
                        </MDBDropdown>
                    </MDBNavItem>
                        <MDBNavItem>
                            <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Orders">Orders</NavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Contact">Contact</NavLink>
                        </MDBNavItem>
                       

                </MDBNavbarNav>
                <MDBNavbarNav right>


                    <div className="md-form my-0">
                    <MDBNavItem>
                            <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Cart"><FontAwesomeIcon icon={faShoppingCart} /></NavLink>
                        </MDBNavItem>
                    </div>


                </MDBNavbarNav>
               
            </MDBCollapse>
        </MDBNavbar>);
        }
}

MenuBar.defaultProps = {
    categoryList: []
  };