import Network from './Api';

export const getCategories = () => {
    return async dispatch => {
        
        try {
            
            dispatch({ type: "CATEGORY_LOADING" });
            let response = await Network.get('/Request.php', {
                params: {
                    type: 'GET_CATEGORIES'
                }
            });
           
            dispatch({ type: "GET_CATEGORIES_SUCCESS", payload: response.data });

        } catch (error) {
            // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
        }
    }
}


