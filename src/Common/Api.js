import Axios from 'axios';

const baseApiUrl = "https://www.teenagefashion.in/API";
const instance = Axios.create({
baseURL: baseApiUrl,
timeout: 30000
});

export default instance;