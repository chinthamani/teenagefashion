import React from 'react';
import logo from '../resources/img/logo.png';
import { Link } from 'react-router-dom';
import MenuBar from './MenuBar';

function Header(props) {
    return (
       
        <header className="header-section">
            <div className="header-top">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-2 text-center text-lg-left mt-2">

                            <Link to="/Home" className="site-logo">
                                <img src={logo} alt="" />
                            </Link>
                        </div>
                        <div className="col-xl-6 col-lg-5">
                            <form className="header-search-form">
                                <input type="text" placeholder="Search on Teenage Fashion ...." />
                                <button><i className="flaticon-search"></i></button>
                            </form>
                        </div>
                        <div className="col-xl-4 col-lg-5">
                            <div className="user-panel">
                                <div className="up-item">
                                    <i className="flaticon-profile"></i>
                                    <a href="/Home">Sign In</a>  or <a href="/Home">Create Account</a>
                                </div>
                                {/* <div className="up-item">
                                    <div className="shopping-card">
                                        <i className="flaticon-bag"></i>
                                        <span>0</span>
                                    </div>
                                    <a href="/Home">Shopping Cart</a>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <MenuBar categoryList={props.categoryList}/>
        </header>
    );

}
export default Header;