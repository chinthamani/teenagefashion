import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import {ClipLoader } from 'react-spinners';

import { Link } from 'react-router-dom';

const override = `
    display: block;
    margin: auto;
    border-color: red;
`;


export default function ProductList(props) {
    return (
        <div className="ProductList">

            <div className="row">
                
                {!props.isLoading?(props.prodList!==undefined?props.prodList.map((datas, index) => {
                    const { id, name, price, imageName,code } = datas;
                    return (
                        <div key={index} className="col-lg-4 col-sm-6">
                            <div className="product-item">
                                <div className="pi-pic">
                                    <div className="tag-sale">ON SALE</div>
                                    <Link to={{
                                        pathname: `/Product/${code}`,
                                        state: {
                                            id
                                        }
                                    }}><img src={"https://www.teenagefashion.in/API/ProductImages/" + imageName} height="250px" width="250px" alt="" /></Link>
                                    <div className="pi-links">
                                        <Link to="/" onClick={(e)=>props.addToCart(e,id)} className="add-card"><FontAwesomeIcon icon={faShoppingCart} /><span>ADD TO CART</span></Link>
                                    </div>
                                </div>
                                <div className="pi-text">
                                    <h6>₹ {price} /-</h6>
                                    <p>{name}</p>
                                </div>
                            </div>
                        </div>
                    );
                }):null): <ClipLoader
                css={override}
                sizeUnit={"px"}
                size={100}
                color={'#123abc'}
                loading={props.isLoading}
              />
                }
            </div>

        </div>
    );
}
ProductList.defaultProps = {prodList:[]};