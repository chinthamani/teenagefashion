function ProductReducer(state = {}, actions) {

    switch (actions.type) {
        case "GET_PRODUCT_LIST_SUCCESS":
            console.log({prodList: actions.payload,isLoading:false});
            return state ={prodList: actions.payload,isLoading:false};
            
        case "GET_PRODUCT_DETAILS_SUCCESS":
            return state = actions.payload;
        case "PRODUCT_LOADING":
            return state= {
                isLoading : true,
                prodList : []
            };
        default: return state;
    }
}

export default ProductReducer;
