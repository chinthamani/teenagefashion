import React from 'react';
import { Link } from 'react-router-dom';
import {ClipLoader } from 'react-spinners';
const override = `
    display: block;
    margin: 0 auto;
    
    border-color: red;
`;
export default function ProductFilter(props) {
    return (
        <div className="ProductFilter">
            <div className="filter-widget">
                <h2 className="fw-title">Categories</h2>
                <ul className="category-menu catMenu">
                    {!props.isLoading?props.catList.map((datas, index) => {
                        const { id, name } = datas;
                        return (
                            <li key={id}><Link to={{
                                pathname: `/Categories/${name}`,
                                state: {
                                    id
                                }
                            }}>{name}</Link></li>
                        );
                    }): <ClipLoader
                    css={override}
                    sizeUnit={"px"}
                    size={100}
                    color={'#123abc'}
                    loading={true}
                  />
                    }
                </ul>
            </div>
        </div>
    );
}
ProductFilter.defaultProps = {catList:[]};