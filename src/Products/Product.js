import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { getProductDetails } from '../Products/ProductActions';
import {ClipLoader} from 'react-spinners';
const override = `
    display: block;
    margin: auto;
    border-color: red;
`;
let mainImage = "";
class Product extends React.Component {

    constructor(props) {
        super(props);
        const id = this.props.location.state.id;
        this.props.getProductDetails(id);
        this.state = {
            loading:false,
            size : "S",
            quantity : "1"
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.product.imageName !== undefined) {
            this.setState({
                loading:true
            });
            nextProps.product.imageName.map((datas, index) => {
              if (index===0)
                mainImage=datas;
                return null;
            });
        }
    }

    handleOptionChange(e){
        this.setState({
            size: e.target.value
        });
      }
      handleSelectChange(e){
        this.setState({
            quantity: e.target.value
        });
      }

    render() {
        return (
            <div className="Product container-fluid">

                <div className="row mt-3 prdImageDiv">
                    <div className="col-lg-6">
                        <div className="product-pic-zoom">
                            {this.state.loading?
                            <img className="product-big-img prdImage"  height="500" src={"https://www.teenagefashion.in/API/ProductImages/" + mainImage} alt="" />:<ClipLoader
                            css={override}
                            sizeUnit={"px"}
                            size={100}
                            color={'#123abc'}
                            loading={true}
                          />}
                        </div>
                        <div className="product-thumbs" tabIndex="1" style={{ overflow: 'hidden', outline: 'none' }}>
                            <div className="product-thumbs-track">
                            {this.state.loading?this.props.product.imageName.map((datas, index) => {
                                return (
                                    index===0?<div key={index} className="pt active" data-imgbigurl={"https://www.teenagefashion.in/API/ProductImages/" + datas}><img src={"https://www.teenagefashion.in/API/ProductImages/" + datas} alt="" /></div>:<div key={index} className="pt" data-imgbigurl={"https://www.teenagefashion.in/API/ProductImages/" + datas}><img src={"https://www.teenagefashion.in/API/ProductImages/" + datas} alt="" /></div>
                                );
                            }):null}
                               
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 product-details">
                        <h2 className="p-title">{this.props.product.name}</h2>
                        <h3 className="p-price">₹ {this.props.product.price}</h3>
                        <h4 className="p-stock">Available: <span>In Stock</span></h4>
                        <div className="p-rating">
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o fa-fade"></i>
                        </div>
                        <div className="p-review">
                            {/* <a href="/">3 reviews</a> */}
                        </div>
                        <div className="fw-size-choose">
                            <p>Size</p>
                            <div className="sc-item">
                                <input type="radio" value="S" checked={this.state.size === 'S'} name="sc" id="s-size" onChange={(e)=>this.handleOptionChange(e)} />
                                <label htmlFor="s-size">S</label>
                            </div>
                            <div className="sc-item">
                                <input type="radio" name="sc" id="m-size" value="M" checked={this.state.size === 'M'} onChange={(e)=>this.handleOptionChange(e)} />
                                <label htmlFor="m-size">M</label>
                            </div>
                            <div className="sc-item">
                                <input type="radio" name="sc" id="l-size" value="L" checked={this.state.size === 'L'} onChange={(e)=>this.handleOptionChange(e)} />
                                <label htmlFor="l-size">L</label>
                            </div>
                            <div className="sc-item">
                                <input type="radio" name="sc" id="xl-size"  value="XL" checked={this.state.size === 'XL'} onChange={(e)=>this.handleOptionChange(e)} />
                                <label htmlFor="xl-size">XL</label>
                            </div>
                            <div className="sc-item">
                                <input type="radio" name="sc" id="xxl-size" value="XXL" checked={this.state.size === 'XXL'} onChange={(e)=>this.handleOptionChange(e)} />
                                <label htmlFor="xxl-size">XXL</label>
                            </div>
                            
                        </div>
                        <div className="quantity">
                            <p>Quantity</p>
                            <div className="pro-qty1"><select value={this.state.quantity} onChange={(e)=>this.handleSelectChange(e)} className="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option></select></div>
                        </div>
                        <a href="/" className="site-btn">SHOP NOW</a>
                        <div id="accordion" className="accordion-area">
                            <div className="panel">
                                <div className="panel-header" id="headingOne">
                                    <button className="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">information</button>
                                </div>
                                <div id="collapse1" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div className="panel-body">
                                        <p>{this.props.product.description}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="panel">
                                <div className="panel-header" id="headingTwo">
                                    <button className="panel-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">care details </button>
                                </div>
                                <div id="collapse2" className="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div className="panel-body">
                                        <img src="./img/cards.png" alt="" />
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra tempor so dales. Phasellus sagittis auctor gravida. Integer bibendum sodales arcu id te mpus. Ut consectetur lacus leo, non scelerisque nulla euismod nec.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="panel">
                                <div className="panel-header" id="headingThree">
                                    <button className="panel-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">shipping & Returns</button>
                                </div>
                                <div id="collapse3" className="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div className="panel-body">
                                        <h4>7 Days Returns</h4>
                                        <p>Cash on Delivery Available<br />Home Delivery <span>3 - 4 days</span></p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra tempor so dales. Phasellus sagittis auctor gravida. Integer bibendum sodales arcu id te mpus. Ut consectetur lacus leo, non scelerisque nulla euismod nec.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        product: state.product,
        cart : state.cart
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getProductDetails
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);