import Network from '../Common/Api';

export const getProducts = (id) => {
    return async dispatch => {
        
        try {
            dispatch({ type: "PRODUCT_LOADING" });
		   
            let response = await Network.get('/Request.php', {
                params: {
                    type: 'GET_PRODUCTS_LIST',
                    id
                }
            });
           
            dispatch({ type: "GET_PRODUCT_LIST_SUCCESS", payload: response.data });

        } catch (error) {
            // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
        }
    }
}

export const getProductDetails = (id) => {
    return async dispatch => {
        
        try {
            dispatch({ type: "PRODUCT_LOADING", payload: true });
            let response = await Network.get('/Request.php', {
                params: {
                    type: 'GET_PRODUCT_DETAILS',
                    id
                }
            });
           
            dispatch({ type: "GET_PRODUCT_DETAILS_SUCCESS", payload: response.data });

        } catch (error) {
            // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
        }
    }
}


