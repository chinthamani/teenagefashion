import React from 'react';
import prod1 from '../resources/img/product/1.jpg';
import prod2 from '../resources/img/product/2.jpg';
import prod3 from '../resources/img/product/3.jpg';
import prod4 from '../resources/img/product/4.jpg';

function LatestProducts() {
    return (
        <div className="LatestProducts">
            <section className="top-letest-product-section">
                <div className="container">
                    <div className="section-title">
                        <h2>LATEST PRODUCTSsssssssssssssssss nila</h2>
                    </div>
                    <div className="product-slider row">
                        <div className="product-item col-md-3">
                            <div className="pi-pic">
                                <img src={prod1} alt="" />
                                <div className="pi-links">
                                    <a href="/Home" className="add-card"><i className="flaticon-bag"></i><span>ADD TO CART</span></a>
                                    <a href="/Home" className="wishlist-btn"><i className="flaticon-heart"></i></a>
                                </div>
                            </div>
                            <div className="pi-text">
                                <h6>Rs.35,00</h6>
                                <p>Flamboyant Pink Top </p>
                            </div>
                        </div>
                        <div className="product-item col-md-3">
                            <div className="pi-pic">
                                <div className="tag-new">New</div>
                                <img src={prod2} alt="" />
                                <div className="pi-links">
                                    <a href="/Home" className="add-card"><i className="flaticon-bag"></i><span>ADD TO CART</span></a>
                                    <a href="/Home" className="wishlist-btn"><i className="flaticon-heart"></i></a>
                                </div>
                            </div>
                            <div className="pi-text">
                                <h6>Rs.35,00</h6>
                                <p>Black and White Stripes Dress</p>
                            </div>
                        </div>
                        <div className="product-item col-md-3">
                            <div className="pi-pic">
                                <img src={prod3} alt="" />
                                <div className="pi-links">
                                    <a href="/Home" className="add-card"><i className="flaticon-bag"></i><span>ADD TO CART</span></a>
                                    <a href="/Home" className="wishlist-btn"><i className="flaticon-heart"></i></a>
                                </div>
                            </div>
                            <div className="pi-text">
                                <h6>Rs.35,00</h6>
                                <p>Flamboyant Pink Top </p>
                            </div>
                        </div>
                        <div className="product-item col-md-3">
                            <div className="pi-pic">
                                <img src={prod4} alt="" />
                                <div className="pi-links">
                                    <a href="/Home" className="add-card"><i className="flaticon-bag"></i><span>ADD TO CART</span></a>
                                    <a href="/Home" className="wishlist-btn"><i className="flaticon-heart"></i></a>
                                </div>
                            </div>
                            <div className="pi-text">
                                <h6>Rs.35,00</h6>
                                <p>Flamboyant Pink Top </p>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    );
}
export default LatestProducts;