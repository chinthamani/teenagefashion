import React, { Component } from 'react';
import LatestProducts from '../Home/LatestProducts';
import TopProducts from '../Home/TopProducts';
class Home extends Component{
    render(){
        return (
            <div className="Home">
                
               <LatestProducts/>
               <TopProducts/>
            </div>
        );
    }
}
export default Home;